# snippets

Some useful code snippets for some common use cases.

These code snippets are provided without any warranty of any kind. Use at your own risks.
That being said, they should be quite safe to use.
