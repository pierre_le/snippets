#!/bin/bash
#
# Contains commonly used commands regarding Docker
# Source this file from your own ".bash_aliases".
#

# Note that this `dc` alias conflicts with the actual `dc` command, that provides a command-line calculator. Feel free to change the name of this alias.
alias dc="docker compose"

function dshell()
{
    if [ -z $1 ]
    then
        >&2 echo "Container name or ID is required."
    else
        docker exec -it "$1" bash
    fi
}
