#!/bin/bash
#
# Used in conjuction with the "dshell" alias.
# Allows completion based on the container names, as reported by `docker ps`.
# Copy this file to "/etc/bash_completion.d/".
#
_dshell()
{
    local alloptions=`docker ps --format '{{.Names}}' | sed ':a;N;$!ba;s/\n/ /g'`
    local cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "$alloptions" -- $cur) )
}
complete -F _dshell dshell

#
# Allows autocomplete on the `dc` alias.
# Prerequisite: install the official bash completion script from "https://docs.docker.com/compose/completion/"
#
complete -F _docker_compose dc
